import styles from "./product-read-more.module.scss";

const ProductReadMore = ({ copy }) => {
  return (
    <div className={styles.productReadMore}>
      <div>Read more</div>
      <div>&gt;</div>
      <p dangerouslySetInnerHTML={{ __html: copy.title }}></p>
    </div>
  );
};

export default ProductReadMore;
